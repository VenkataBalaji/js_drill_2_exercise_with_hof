//3. Assume that each salary amount is a factor of 10000 and correct it but add it as a new key (corrected_salary or something)


function convertSalary(employee) {
    let salaryNumber = parseFloat(employee.salary.replace("$", ""));
    if (!isNaN(salaryNumber)) {
      employee.corrected_salary = salaryNumber * 10000;
      return employee;
    } else {
      console.log(" Invalid salary format.");
      return null;
    }
  }
  
  function processSalaries(data) {
    if (!Array.isArray(data)) {
      console.log(" Data is not an array.");
      return null;
    }
  
    const processedData = data.map((employee) => convertSalary(employee)).filter((employee) => employee !== null);
  
    return processedData.length > 0 ? processedData : null;
  }
  
  module.exports = processSalaries;
  