//  4. Find the sum of all salaries.


function getSumOfSalaries(data) {
    if (!Array.isArray(data)) {
      console.log("Data Insufficient.");
      return null;
    }
  
    const totalSalary = data.reduce((sum, employee) => {
      const salary = parseFloat(employee.salary.replace("$", ""));
      if (!isNaN(salary)) {
        return sum + salary;
      } else {
        console.log("Invalid salary format.");
      
      }
    }, 0);
  
    return totalSalary;
  }
  
  module.exports = getSumOfSalaries;
  