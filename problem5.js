//5. Find the sum of all salaries based on country. ( Group it based on country and then find the sum ).



function getSumOfSalariesBasedOnCountry(data) {
    if (!Array.isArray(data)) {
      console.log("Data InSuffient.");
      return null;
    }
  
    let salaryByCountry = data.reduce((acc, employee) => {
      let salary = parseFloat(employee.salary.replace("$", ""));
      let country = employee.location;
  
      if (!isNaN(salary)) {
        acc[country] = (acc[country] || 0) + salary;
      } else {
        console.log(" Invalid salary format.");
       
      }
  
      return acc;
    }, {});
  
    return salaryByCountry;
  }
  
  module.exports = getSumOfSalariesBasedOnCountry;
  