function getAvgSalariesBasedOnCountry(data) {
    if (!Array.isArray(data)) {
      console.log("Error: Data Insuffient.");
      return null;
    }
  
    let averageSalaryByCountry = {};
    let countByCountry = {};
  
    data.forEach((employee) => {
      let salary = parseFloat(employee.salary.replace("$", ""));
      let country = employee.location;
  
      if (!isNaN(salary)) {
        averageSalaryByCountry[country] = (averageSalaryByCountry[country] || 0) + salary;
        countByCountry[country] = (countByCountry[country] || 0) + 1;
      } else {
        console.log(" Invalid salary format.");
      }
    });
  
    for (const country in averageSalaryByCountry) {
      averageSalaryByCountry[country] /= countByCountry[country];
    }
  
    return averageSalaryByCountry;
  }
  
  module.exports = getAvgSalariesBasedOnCountry;
  