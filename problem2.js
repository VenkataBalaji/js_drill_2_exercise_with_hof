//2. Convert all the salary values into proper numbers instead of strings.


function convertSalariesToNumber(data) {
    if (!Array.isArray(data)) {
      console.log(" Data Insufficient.");
      return null;
    }
  
    let newData = data.map((employee) => {
      let salary = parseFloat(employee.salary.replace("$", ""));
      if (!isNaN(salary)) {
        return { ...employee, salary };
      } else {
        console.log("Invalid salary format.");
          }
    });
  
    return newData;
  }
  
  module.exports = convertSalariesToNumber;
  