//1. Find all Web Developers. ( It could be Web Developer III or Web Developer II or anything else )


function findWebDevelopers(employees) {
    if (!Array.isArray(employees)) {
      return " Employees data is not an array.";
    }
  
    return employees.filter((employee) => employee.job.includes("Web Developer"));
  }
  
  module.exports = findWebDevelopers;
  